#install basic programs (part 2)
# ranger and nemo (file manager)
# firefox (browser)
# mesa-utils mesa-utils-extra (3D library)
# pulseaudio (sound server)
# nitrogen (wallpaper manager)
# neofetch (general system information)
# redshift (color temperature)
# htop (process viewer)
# xbacklight
# mpv (video player)
# git
# vim
# gimp (image editor)
# transmission-gtk (BitTorrent client)

apt-get install libreoffice ranger rofi firefox mesa-utils mesa-utils-extra pulseaudio nitrogen neofetch redshift htop xbacklight mpv git vim gimp transmission-gtk

bc
lm-sensors
feh
scrot
qalc
imagemagick
snapd
ncmpcpp
mpc
sxiv
openssh-server
    systemctl status ssh                # Verifica se está ativo
    sudo ufw allow ssh                  # Libera no firewall
    sudo vim /etc/ssh/sshd_config       # Entra no arquivo de configuração para alterar a porta
    sudo systemctl restart ssh.service  # Reinicia o ssh
texlive-full
texmaker
ffmpegthumbnailer                       # Preview de videos no ranger
# Caso o ncmpcpp não carregue as música, basta deletar o arquivo '~/.config/mpd/database'
# Desativar a inicialização automática dele utilizando o comando 'sudo systemctl disable mpd'

# Caso não funcione, executar as seguintes etapas:
# Copy /etc/xdg/autostart/mpd.desktop to ~/.config/autostart/mpd.desktop
# Edit ~/.config/autostart/mpd.desktop to change X-GNOME-Autostart-enabled=true to X-GNOME-Autostart-enabled=false, or delete the line
# Edit or add a line `Hidden=true
# fonte: https://askubuntu.com/questions/1148162/prevent-mpd-finally-from-auto-starting
# Outros
steam

# Snap
spotify
#part 2
#config folders and files(i3, ranger, urxvt)
cp -R i3 ~/.config
cp -R ranger ~/.config
cp .Xdefaults ~/
mkdir ~/.fonts
cp fonts/* ~/.fonts

#Configurations
# temperature
